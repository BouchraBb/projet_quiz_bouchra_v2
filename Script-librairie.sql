-- creation table

create table livre 
(
numl numeric not null,
titre varchar(20),
nbPages numeric,
anneeImpression date
);
commit; 

alter table livre
add constraint pk_livre primary key (numl);
commit; 


create table auteur 
(
numaut numeric not null,
nom varchar(20),
prenom varchar(20)
);
commit; 



alter table auteur
add constraint pk_auteur primary key (numaut);
commit; 


create table editeur
(
numE numeric not null,
nom varchar(20),
ville varchar(20)
);
commit;

alter table editeur
add constraint pk_editeur primary key (nume);
commit;

create table ecrit
(
numl int  REFERENCES livre(numl),
numaut int  REFERENCES auteur(numaut)
);
commit;

alter table ecrit
add constraint pk_ecrit primary key (numl, numaut);
commit;


create table edite
(
numl int  REFERENCES livre(numl),
nume int  REFERENCES editeur(nume)
);
commit;

alter table edite
add constraint pk_edite primary key (numl, nume);
commit;


-- init data
-- auteur
INSERT INTO public.auteur
(numaut, nom, prenom)
VALUES(1, 'HIMES', 'Chester');

INSERT INTO public.auteur
(numaut, nom, prenom)
VALUES(2, 'BAZIN', 'Herve');

INSERT INTO public.auteur
(numaut, nom, prenom)
VALUES(3, 'COHEN', 'Albert');
commit; 


--livre
INSERT INTO public.livre
(numl, titre, nbpages, anneeimpression)
VALUES(1, 'La reine des pommes', 282, '01-01-2003');

INSERT INTO public.livre
(numl, titre, nbpages, anneeimpression)
VALUES(2, 'Mangeclous', 498, '01-01-2004');


INSERT INTO public.livre
(numl, titre, nbpages, anneeimpression)
VALUES(3, 'Belle du seigneur', 1110, '01-01-2002');

INSERT INTO public.livre
(numl, titre, nbpages, anneeimpression)
VALUES(4, 'Couch´e dans le pain', 248, '01-01-2002');

INSERT INTO public.livre
(numl, titre, nbpages, anneeimpression)
VALUES(5, 'Le Matrimoine', 286, '01-01-1976');
commit;

-- editeur 
INSERT INTO public.editeur (nume, nom, ville) VALUES(1, 'Gallimard', 'Paris');
INSERT INTO public.editeur (nume, nom, ville) VALUES(2, 'Seuil', 'lille');
commit;
select * from editeur 

-- ecrit 
INSERT INTO public.ecrit (numl, numaut) VALUES(1, 1);
INSERT INTO public.ecrit (numl, numaut) VALUES(2, 3);
INSERT INTO public.ecrit (numl, numaut) VALUES(3, 3);
INSERT INTO public.ecrit (numl, numaut) VALUES(4, 1);
INSERT INTO public.ecrit (numl, numaut) VALUES(5, 2);
commit;

--edite
INSERT INTO public.edite (numl, nume) VALUES(1, 1);
INSERT INTO public.edite (numl, nume) VALUES(2, 1);
INSERT INTO public.edite (numl, nume) VALUES(3, 1);
INSERT INTO public.edite (numl, nume) VALUES(4, 1);
INSERT INTO public.edite (numl, nume) VALUES(5, 2);
commit;




