create table employe
(
	noemp numeric not null,
	nom VARCHAR(20),
	prenom VARCHAR(20),
	emploi VARCHAR(20),
	sup numeric,
	embauche date,
	sal numeric,
	comm numeric
);

alter table employe
add constraint PK_EMP primary key (noemp);


create table service
(
noserv numeric not null,
service varchar(20),
ville varchar(20)
);

2- alter table service
add constraint PK_SERV primary key(noserv);

create table travaille
(
noemp int  REFERENCES employe(noemp),
noserv int  REFERENCES service(noserv),
date_debut date,
date_fin date
);
 
alter table travaille
add constraint PK_travaille primary key(noemp,noserv);

create table projet
(
noproje numeric not null,
nomproj varchar(20),
budget numeric(12,2)
);

 1- alter table projet
add constraint PK_PROJET primary key (noproje);

create table participe
(
noemp int  REFERENCES employe(noemp),
noproje int  REFERENCES projet(noproje),
date_debut date,
date_fin date
);


alter table participe
add constraint PK_PARTICIPE primary key(noemp,noproje);


3- alter table projet 
add constraint ck_bugdet check( BUDGET >20000);

4- alter table employe
add constraint fk1_employe foreign key (SUP) references
EMPloye(NOEMP);

5- alter table employe 
add constraint  ck_sup check (sup between 0 and 9999);

6- alter table service 
add constraint  ck_noserv check (noserv<10);

insert into employe values (1000,'LEROY','PAUL','PRESIDENT',null,to_date('25/10/87','dd/MM/yy'),55005.5,null);
insert into employe values (1300,'LENOIR','GERARD','DIRECTEUR',1000,to_date('02/04/87','dd/MM/yy'),31353.14,13071);
insert into employe values (1200,'LEMAIRE','GUY','DIRECTEUR',1000,to_date('11/03/87','dd/MM/yy'),36303.63,null);
insert into employe values (1500,'DUPONT','JEAN','DIRECTEUR',1000,to_date('23/10/87','dd/MM/yy'),28434.84,null);
insert into employe values (1600,'LAVARE','PAUL','DIRECTEUR',1000,to_date('13/12/91','dd/MM/yy'),31238.12,null);

insert into employe values (1100,'DELPIERRE','DOROTHEE','SECRETAIRE',1000,to_date('25/10/87','dd/MM/yy'),12351.24,null);
insert into employe values (1101,'DUMONT','LOUIS','VENDEUR',1300,to_date('25/10/87','dd/MM/yy'),9047.9,0);
insert into employe values (1102,'MINET','MARC','VENDEUR',1300,to_date('25/10/87','dd/MM/yy'),8085.81,17230);
insert into employe values (1104,'NYS','ETIENNE','TECHNICIEN',1200,to_date('25/10/87','dd/MM/yy'),12342.23,null);
insert into employe values (1105,'DENIMAL','JEROME','COMPTABLE',1600,to_date('25/10/87','dd/MM/yy'),15746.57,null);
insert into employe values (1201,'MARTIN','JEAN','TECHNICIEN',1200,to_date('25/06/87','dd/MM/yy'),11235.12,null);
insert into employe values (1202,'DUPONT','JACQUES','TECHNICIEN',1200,to_date('30/10/88','dd/MM/yy'),10313.03,null);
insert into employe values (1301,'GERARD','ROBERT','VENDEUR',1300,to_date('16/04/99','dd/MM/yy'),7694.77,12430);
insert into employe values (1303,'MASURE','EMILE','TECHNICIEN',1200,to_date('17/06/88','dd/MM/yy'),10451.05,null);
insert into employe values (1501,'DUPIRE','PIERRE','ANALYSTE',1500,to_date('24/10/84','dd/MM/yy'),23102.31,null);
insert into employe values (1502,'DURAND','BERNARD','PROGRAMMEUR',1500,to_date('30/07/87','dd/MM/yy'),13201.32,null);
insert into employe values (1503,'DELNATTE','LUC','PUPITREUR',1500,to_date('15/01/99','dd/MM/yy'),8801.01,null);
insert into employe values (1601,'CARON','ALAIN','COMPTABLE',1600,to_date('16/09/85','dd/MM/yy'),33003.3,null);
insert into employe values (1602,'DUBOIS','JULES','VENDEUR',1300,to_date('20/12/90','dd/MM/yy'),9520.95,35535);
insert into employe values (1603,'MOREL','ROBERT','COMPTABLE',1600,to_date('18/07/85','dd/MM/yy'),33003.3,null);
insert into employe values (1604,'HAVET','ALAIN','VENDEUR',1300,to_date('01/01/91','dd/MM/yy'),9388.94,33415);
insert into employe values (1605,'RICHARD','JULES','COMPTABLE',1600,to_date('22/10/85','dd/MM/yy'),33503.35,null);
insert into employe values (1615,'DUPREZ','JEAN','BALAYEUR',1000,to_date('22/10/98','dd/MM/yy'),6000.6,null);

commit;


insert into service values (1,'DIRECTION','PARIS');
insert into service values (2,'LOGISTIQUE','SECLIN');
insert into service values (3,'VENTES','ROUBAIX');
insert into service values (4,'FORMATION','VILLENEUVE D''ASCQ');
insert into service values (5,'INFORMATIQUE','LILLE');
insert into service values (6,'COMPTABILITE','LILLE');
insert into service values (7,'TECHNIQUE','ROUBAIX');


commit;

 select * from employe
create index numemp on employe (noemp);

drop index numemp;

