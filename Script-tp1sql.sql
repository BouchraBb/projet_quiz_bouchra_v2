1-  SELECT * from serv;
2-  SELECT noserv, service, ville from serv;
3- select  SERVICE, NOSERV from serv;
4- SELECT * from emp;
5-  SELECT emploi from emp;
6-  SELECT distinct emploi from emp; 
7-  SELECT emploi from emp where noserv ='3';
8-  select nom, prenom, noemp , noserv from emp where emploi='TECHNICIEN';
9-  select service, noserv from serv where noserv>2;
10- select service, noserv from serv where noserv<=2;
11- select * from emp where coalesce(comm, 0)<sal;
12- select * from emp where comm isnull;
13- select * from emp where comm is notnull order by comm asc ;
14- select * from emp where sup notnull;
15- select * from emp where sup isnull;
16- select  nom, emploi, sal, noserv from emp where (noserv=5 and sal>20000);
17- SELECT* from emp where (emploi='VENDEUR' and noserv=6 and (sal+comm)>9500);
18- select * from emp where (emploi='PRESIDENT' or emploi='DIRECTEUR');
19- select * from emp where (emploi='DIRECTEUR'and (noserv!=3));
20- select * from emp where (emploi='DIRECTEUR' or (emploi='TECHNICIEN' and noserv=1));

21- select * from emp where ((emploi='DIRECTEUR' or emploi='TECHNICIEN') and noserv=1);
22- select * from emp where (noserv=1 and (emploi='DIRECTEUR' or emploi='TECHNICIEN'));
23- select * from emp where (noserv=1 and emploi!='DIRECTEUR' and emploi!='TECHNICIEN');
24- select * from emp where emploi in ('TECHNICIEN','COMPTABLE', 'VENDEUR');
25- select * from emp where (emploi!='TECHNICIEN' and emploi!='COMPTABLE' and  emploi!='VENDEUR');
26- select * from emp where (emploi='DIRECTEUR' and (noserv=2 or noserv=4 or noserv=5));
27- select * from emp where( sup is notnull  and noserv!=1 and noserv!=3 and noserv!=5 );
28- select * from emp where (sal+ coalesce(comm, 0) ) between 20000 and 40000;
29- select * from emp where (sal+ coalesce(comm, 0 ))<20000 or (sal+ coalesce(comm, 0))>40000;
30- select * from emp where ( emploi!= 'DIRECTEUR' and embauche between '1988-01-01' and '1988-12-31');
31- select * from emp where ( emploi= 'DIRECTEUR' and ( noserv between 2 and 5));
32- select * from emp where nom like 'M%';
33- select * from emp where nom like '%T';
34- select * from emp where nom like '%E%E%';
35-select nom, prenom, emploi, noserv
from emp
where nom like '%E%'
Except
select nom, prenom, emploi, noserv
from emp
where nom like '%E%E%';   
35-bis  select * from emp where LENGTH(emp.nom) - LENGTH(REPLACE(emp.nom, 'E', '')) = 1 ;
36- select * from emp where (nom like '%N%' or nom like '%O%');
37- select * from emp where ( length(nom)=6 and nom like '%N');
38- select * from emp where nom like '___T%';
39- select * from emp where  length(nom)!=5 ;

40- select nom , prenom, noserv, sal from emp where noserv=3 order by sal asc ;
41- select nom , prenom, noserv, sal from emp where noserv=3 order by sal desc ;
42- select nom , prenom, noserv, sal from emp where noserv=3 order by 4 desc ;
43  select nom , prenom, noserv, sal, emploi  from emp order by emploi , sal desc ;
44- select nom , prenom, noserv, sal, emploi  from emp order by 5, 4 desc ;
45- select nom , prenom, noserv,comm  from emp where noserv= 3 order by  4 asc ;
46- select nom , prenom, noserv, coalesce(comm, 0) as comm  from emp where noserv= 3 order by  4 asc ;
 
47- SELECT  emp.nom, emp.prenom , emp.emploi  , serv.service  FROM emp
inner JOIN serv ON emp.noserv = serv.noserv;

48- SELECT  emp.nom  ,  emp.emploi ,emp.noserv , serv.service  FROM emp
inner JOIN serv ON emp.noserv = serv.noserv;

49- SELECT  e.nom  ,  e.emploi ,e.noserv , s.service  FROM emp as e
inner JOIN serv as s ON e.noserv = s.noserv;

50- SELECT  e.nom  ,  e.embauche  , s.* FROM emp as e
inner JOIN serv as s ON e.noserv = s.noserv;

51- SELECT  e.nom  ,  e.embauche  ,t2.nom , t2.embauche  FROM emp as e
left outer  JOIN emp as t2 ON e.sup = t2.noemp  where e.embauche <t2.embauche  order by  e.nom , t2.nom ;

52- select distinct prenom from emp where emploi='DIRECTEUR' union select  prenom from emp where (emploi='TECHNICIEN'and noserv=1);

53- select noserv from serv except select noserv from emp ;

54- select distinct serv.noserv from serv inner join  emp on serv.noserv = emp.noserv ;

54-bis  select  serv.noserv from serv intersect select emp.noserv from emp ;

 select nom, ville  from emp  inner join serv on emp.noserv= serv.noserv ;
55- select * from emp where noserv in (select noserv from serv where ville='LILLE');

56- select * from emp where sup in (select sup from emp where nom='DUBOIS');

57- select * from emp where embauche  in (select embauche  from emp where nom='DUMONT');

58- select nom, embauche  from emp where embauche< (select embauche  from emp where nom='MINET') order by 2;

59- select nom,prenom ,embauche  from emp where embauche < (select min(embauche) from emp where noserv=6);

60- select nom,prenom ,sal+ coalesce (comm,0) as revenus_mensuel from emp where sal+ coalesce (comm,0) > (select max(sal+ coalesce (comm,0)) from emp where noserv=3) order by 3 asc;

61- select nom ,emp.noserv , emploi , sal , ville  from emp cross join  serv where emp.noserv = serv.noserv  and  serv.ville in (select serv.ville from serv, emp where emp.nom='HAVET' and emp.noserv=serv.noserv);

62- (select emploi from emp where noserv= 1) intersect  (select emploi from emp where noserv= 3 ) ;

63-  (select emploi from emp where noserv= 1) except  (select emploi from emp where noserv= 3 ) ;

64- select nom ,prenom,  emploi , sal from emp where (emploi=(select emploi from emp where nom='CARON') and sal>(select sal from emp where nom='CARON') );

65- select * from emp where (noserv =1 and emploi in (select emploi from emp inner join  serv on emp.noserv= serv.noserv  where serv.service ='VENTES') );

66-  select * from emp, serv  where (serv.ville='LILLE' and emploi=(select emploi from emp where nom='RICHARD';) ) order by nom;

• 67 : Sélectionner les employés dont le salaire est plus élevé que le salaire moyen de leur service (moyenne des salaires = avg(sal)), résultats triés
par numéros de service.

67-  select  ( select avg( sal )from emp as em where e.noserv= em.noserv ) as salaire_moyen , * from emp as e where sal > (select avg( sal )from emp as em where e.noserv= em.noserv)    order by e.noserv;

68- select * from emp inner join  serv on emp.noserv= serv.noserv  where serv.service ='INFORMATIQUE'  and extract(year from embauche) in (select extract(year from embauche) as annee from emp inner join  serv on emp.noserv= serv.noserv  where serv.service ='VENTES');

69-  select nom , emploi ,  ville  from emp as e inner join serv on e.noserv=serv.noserv  where e.noserv !=(select em.noserv from emp as em where e.sup= em.noemp );

 Sélectionner le nom, le prénom, le service, le revenu des employés qui ont des subalternes, trier le résultat suivant le revenu décroissant.
 
70- select nom , prenom , service ,  sal+ coalesce (comm,0) as revenus_mensuel from emp as e inner join serv on e.noserv=serv.noserv  where e.noemp  in (select distinct sup from emp where sup notnull ) ;

71- select nom , emploi , ROUND( (sal+ coalesce (comm,0)) , 2) as revenus from emp as e order by revenus desc  ;

72- select nom, sal, comm from emp where coalesce(comm,0) >(2*sal) ;

-- if else if en sql
select e.nom , e.prenom ,e.emploi , e.comm , e.sal, ROUND( (e.sal+ coalesce (e.comm,0)) , 2) as revenus,case
	when comm isnull then ROUND(0*100 , 2)
	when comm!=0 then  ROUND((e.sal+ e.comm)/e.comm ,2  ) 
end
as "%Commissions" from emp as e  where e.emploi= 'VENDEUR' order by 6 desc;

73- select e.nom , e.prenom ,e.emploi , e.comm , e.sal, ROUND( (e.sal+ coalesce (e.comm,0)) , 2) as revenus, ROUND((coalesce(e.comm, 0)*100)/(e.sal+ coalesce (e.comm,0)) ,2  )as "%Commissions" from emp as e  where e.emploi= 'VENDEUR' order by 6 desc;

74- select e.nom , e.prenom ,e.emploi ,s.service, ROUND(( sal+ coalesce (e.comm,0))*12 , 2) as revenu_annuel from emp as e inner join serv as s on e.noserv= s.noserv  where e.emploi= 'VENDEUR';

75- select e.nom ,e.emploi ,e.sal,e.comm,  ROUND( sal+ coalesce (e.comm,0), 2) as revenus_mensuel from emp as e where  e.noserv = 3 ;

76- select e.nom ,e.emploi ,e.sal as SALAIRE ,e.comm as COMMISSION,  ROUND( sal+ coalesce (e.comm,0), 2) as GAIN_MENSUEL from emp as e where  e.noserv = 3 ;

77-  select e.nom ,e.emploi ,e.sal as SALAIRE ,e.comm as COMMISSION,  ROUND( sal+ coalesce (e.comm,0), 2) as "GAIN MENSUEL" from emp as e where  e.noserv = 3 ; 

78- select e.nom ,e.emploi ,(e.sal/22) as SALAIRE_JOURNALIER , (22*8)as horaire from emp as e where  e.noserv = 3;
  - select e.nom ,e.emploi , ROUND((e.sal/22) , 2) as SALAIRE_JOURNALIER , (22*8)as horaire from emp as e where  e.noserv = 3;
 
79- select e.nom ,e.emploi , TRUNC((e.sal/22) , 2) as SALAIRE_JOURNALIER , (22*8)as horaire from emp as e where  e.noserv = 3;

80- select concat(rpad(service, 13, '-' ),'>',ville) as  "service ville " from serv;

81- select nom, emploi,case 
	when emploi='PRESIDENT' then 1
	when emploi='DIRECTEUR' then 2
	when emploi='COMPTABLE' then 3
	when emploi='VENDEUR' then 4
	when emploi='TECHNICIEN' then 5
	when emploi='PROGRAMMEUR' then 6
	else 0
	
end
as "CODE EMPLOI" from emp  order by 3 asc;

82- select noemp, case 
	when noserv=1  then '******'
	else nom
end
as "nom",prenom, emploi, sal, comm, sup,noserv from emp  order by 8 asc;

83- select noserv ,  rpad(service, 5) as service, ville   from serv;

84- select * from emp where  extract (year FROM  embauche )=1988 ;

85- select Upper( nom) as "NOM" , substring(nom, 1,1)||lower(substring(nom, 2)) as "Nom"  , lower(nom) as nom from emp;
  - select Upper( nom) as "NOM" ,initcap(nom)  as "Nom"  , lower(nom) as nom from emp;
 
86- select nom, POSITION('M' in nom )  as premier_m, POSITION('E' in nom )  as premier_e from emp;

87- select service , length(service) as nombre_char from serv;

88- select  rpad(concat(nom,' ', emploi,' ' ,sal , '***'), 30)  as histogramme from emp order by sal ; 

89- select nom, emploi, embauche from emp where noserv= 5;

90- select nom, emploi, to_char(embauche, 'dd-mm-yy')  as embauche  from emp where noserv= 5;

91- select nom, emploi, to_char(embauche, 'fm day dd month yyyy')  as embauche  from emp where noserv= 5;

92- select nom, emploi, to_char(embauche, 'dy mon yy')  as embauche  from emp where noserv= 5;

93- select nom, emploi, to_char(embauche, 'fm Day dd Month yyyy')  as embauche  from emp where noserv= 5;

94- select nom, emploi, to_char(embauche, 'fm day dd month yyyy')  as embauche  from emp where noserv= 5;

95- select nom, emploi, age (current_timestamp , embauche)  as embauche  from emp where noserv= 5;
  - select  nom, emploi, (current_timestamp - embauche)  as embauche  from emp where noserv= 5;
 
96- select nom, emploi, ((current_date  - embauche)/30) as embauche  from emp where noserv= 5;

97- select embauche from emp where  extract (year FROM  current_date) - extract (year FROM  embauche ) >= 12 ;

98- select * from emp where  extract (year FROM  current_date) - extract (year FROM  embauche ) >= 12 ;

99- select (current_date - TIMESTAMP '2019-10-30')  as my_days  ;

100- select date'1994-02-16' + integer '10000' as have_10000_days;

/*111 : Pour chaque service, afficher son N° et le salaire moyen des employés du service
arrondi l’euro près.*/
select noserv, avg(sal) as salaire_moyen 
from emp
group by noserv order by 1;

/*112 : Pour chaque service donner le salaire annuel moyen de tous les employés qui ne
sont ni président, ni directeur.*/
select noserv, avg(sal*12) as salaire_annuel_moyen 
from emp
where emploi!='PRESIDENT' and emploi!='DIRECTEUR'
group by noserv;
/*113 : Grouper les employés par service et par emploi à l'intérieur de chaque service,
pour chaque groupe afficher l'effectif et le salaire moyen.*/
select  noserv, emploi, count(*)  as effectif, avg(sal) as salaire_moyen 
from emp emp
group by noserv ,emploi
order by emploi;

/*114 : Idem en remplaçant le numéro de service par le nom du service.*/
select  service, emploi, count(*)  as effectif, avg(sal) as salaire_moyen 
from emp emp
	inner join serv 
	on emp.noserv = serv.noserv 
group by service ,emploi
order by emploi;

/*115 : Afficher l'emploi, l'effectif, le salaire moyen pour tout type d'emploi ayant plus
de deux représentants.*/
select   emploi, count(*)  as effectif, avg(sal) as salaire_moyen 
from emp emp
	inner join serv 
	on emp.noserv = serv.noserv	
group by emploi
HAVING
        COUNT(*)>2;

/* 116 : Sélectionner les services ayant au mois deux vendeurs.*/
select   service ,emploi, count(*)  as effectif 
from emp emp
	inner join serv 
	on emp.noserv = serv.noserv	
	where emploi='VENDEUR'
group by service, emploi
HAVING
        COUNT(*)>=2;


/*117 : Sélectionner les services ayant une commission moyenne supérieure au quart du
salaire moyen.*/
select   service, count(*)  as effectif, avg(coalesce(comm,0))as commissio_moyen, avg(sal) /4 as quart_sal_moyen 
from emp emp
	inner join serv 
	on emp.noserv = serv.noserv	
group by service
having avg(coalesce(comm,0))> (avg(sal) /4);
/*118 : Sélectionner les emplois ayant un salaire moyen supérieur au salaire moyen des
directeurs.*/
select emploi, avg(sal) as salaire_moyen 
from emp
group by emploi 
having  avg(sal)> (select avg(sal) from emp  where emploi='DIRECTEUR')

/* 119 : Afficher, sur la même ligne, pour chaque service, le nombre d'employés ne
touchant pas de commission et le nombre d'employés touchant une commission.*/
select service, *

/*120 : Afficher l'effectif, la moyenne et le total pour les salaires et les commissions par
emploi.*/
select emp.emploi, count(*) as effectif , avg(sal) as salaire_moyen , sum(sal) as salaire_total,
avg(coalesce(comm,0)) as comm_moyenne, sum(coalesce(comm,0)) as comm_total
from emp 
group by emp.emploi;

/* 121 : Augmenter de 10% ceux qui ont une salaire inférieur au salaire moyen. Valider.(14 rows)*/
update emp 
set sal= sal + sal*0.1
where sal< (select sum(sal)/23 from emp )
select * from emp where sal<19527;
commit;

/*122 : Insérez vous comme nouvel employé embauché aujourd’hui au salaire que vous désirez. Valider*/
INSERT INTO public.emp (noemp, nom, prenom, emploi, sup, embauche, sal, comm, noserv)
VALUES(1616, 'BASSIR', 'Bouchra','CHEF DE PROJET', 1212, current_date , 50000, null, 1);
commit;

select distinct emploi from emp where noserv= 6;

/*123 : Effacer les employés ayant le métier de SECRETAIRE. Valider.(1 delete)*/
delete
from emp 
where emploi='SECRETAIRE'

/*124 : Insérer le salarié dont le nom est MOYEN, prénom Toto, no 1010, embauché le 12/12/99,
supérieur 1000, pas de comm, service no 1, salaire vaut le salaire moyen des employés. Valider.*/
INSERT INTO public.emp (noemp, nom, prenom, emploi, sup, embauche, sal, comm, noserv)
VALUES(1010, 'MOYEN', 'Toto','CHEF DE PROJET', 1000, '12-12-1999' , (select sum(sal)/23 from emp), null, 1);
commit;

/*125 : Supprimer tous les employés ayant un A dans leur nom. Faire un SELECT sur votre table
pour vérifier cela. Annuler les modifications et vérifier que cette action s’est bien déroulée.(12)*/
select *
from emp
where nom like '%A%';

delete
from emp 
where nom like '%A%';
commit;

/*126 : Les verrous. Supprimer l’employé créé à l’exercice 122 de votre voisin. Ne pas valider.
Vérifiez tous les deux le contenu de la table. Demander à votre voisin d’augmenter son propre
salaire de 1000 €. Valider la suppression. Chacun vérifie l’action. Refaire l’exercice en
échangeant les rôles.*/
delete
from emp 
where noemp=1616;

update sal set sal = sal + 1000 from emp where where noemp=1616;

delete
from emp 
where noemp=1616;
commit;

/*• 127 : Créer les tables EMP et SERV comme copie des tables EMP et SERV*/
create  table employes as ( select* from emp);
create  table services as ( select* from serv);

/*128 : Vérifier que la table PROJ n’existe pas*/
ALTER TABLE proj add col1 numeric;

/*129 : Créer une table PROJ avec les colonnes suivantes:
 * numéro de projet (noproj), type numérique 3 chiffres, doit contenir une valeur.
➢ nom de projet (nomproj), type caractère, longueur = 10
➢ budget du projet (budget), type numérique, 6 chiffres significatifs et 2 décimales*/

create table proj (noprojet numeric(3) not null, nomprojet varchar(10), budget numeric(8,2) );

/*130 : Insérer trois lignes de données dans la table PROJ:
• numéros des projets = 101, 102, 103
• noms des projets = alpha, beta, gamma
• budgets = 250000, 175000, 950000
 Afficher le contenu de la table PROJ.
 Valider les insertions faites dans la table PROJ.*/
INSERT INTO public.proj
(noprojet, nomprojet, budget)
VALUES(101, 'alpha', 250000.00);

INSERT INTO public.proj
(noprojet, nomprojet, budget)
VALUES(102, 'beta', 175000.00);

INSERT INTO public.proj
(noprojet, nomprojet, budget)
VALUES(103, 'gamma', 950000.00);

 select * from proj ;

commit;

/*•131 :
Modifier la table PROJ en donnant un budget de 1.500.000 Euros au projet 103.
Modifier la colonne budget afin d'accepter des projets jusque 10.000.000.000 d’Euros
Retenter la modification demandée 2 lignes au dessus.*/

ALTER TABLE proj ALTER COLUMN budget TYPE numeric(13,2);
commit; 
update proj  set budget = budget + 1500000 where noprojet=103; 

/*•132 :
Ajouter une colonne NOPROJ (type numérique) à la table EMP.
Afficher le contenu de la table EMP.*/

ALTER TABLE emp ADD COLUMN noproj numeric(3);
alter table emp drop column noproj;
select * from emp;

/*133 : Affecter les employés du service 2 et les directeurs au projet 101.*/
update emp set noproj = 101 where noserv=2 or emploi='DIRECTEUR';

/*134 : Affecter les employés dont le numéro est supérieur à 1350 au projet 102, sauf ceux qui
sont déjà affectés à un projet.*/
update emp set noproj =102 where noemp>1350 and noproj  notnull ;

/*135 : Affecter les employés n'ayant pas de projet au projet 103*/
update emp set noproj =103 where noproj is null ;


/*•136 : Sélectionner les noms d'employés avec le nom de leur projet et le nom de leur service.*/
select emp.nom, proj.nomprojet  , serv.service from emp, serv, proj where emp.noserv = serv.noserv and emp.noproj= proj.noprojet ;

/*•137 : La liste des tables de l’utilisateur se trouve dans la table système user_tables. Ecrire une
requête qui affiche les noms de toutes vos tables.*/
SELECT tablename
FROM pg_tables

select * from user_tables where tablename not like 'user_%'

/*•138 : Créer la vue EMP1 de la table EMP contenant les colonnes:
numéro d'employé, nom et emploi et limitée aux employés du
service numéro 1,*/
create view EMP1
as select * from emp
where noserv =1;


/*•139 : Sélectionner cette vue EMP1 entièrement,*/


/*•140 : Sélectionner les vendeurs de la vue emp1.*/
select* from emp1 where emploi= 'VENDEUR';

/*•141 : Modifier la table EMP en transformant l'emploi de NYS en
comptable.*/
update emp set emploi='COMPTABLE' where nom='NYS';

/*•142 : Sélectionner la vue EMP1.*/
select * from emp1;







