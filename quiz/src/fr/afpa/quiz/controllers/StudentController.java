package fr.afpa.quiz.controllers;

import java.util.Date;

import fr.afpa.quiz.MainQuiz;
import fr.afpa.quiz.models.entities.Exam;
import fr.afpa.quiz.models.entities.Quiz;
import fr.afpa.quiz.models.entities.Student;
import fr.afpa.quiz.services.IQuizService;
import fr.afpa.quiz.services.impl.ExamServiceIMP;
import fr.afpa.quiz.services.impl.QuizServiceIMP;

public class StudentController {

	private IQuizService quizService = new QuizServiceIMP();
	private ExamServiceIMP examService = new ExamServiceIMP();

	public String principalMenu() {
		String choice;
		do {
			System.out.println("taper 1 => commencer un examen  ");
			System.out.println("taper 2 => afficher tous vos examens realises ");
			System.out.println("taper 3 => quitter l'application ");

			choice = MainQuiz.sc.nextLine();
		} while (!("1".equals(choice)) && !("2".equals(choice)) && !("3".equals(choice)));

		return choice;
	}

	public void startExam(Student student) {

		quizService.dispalyAllQuiz();
		System.out.println("Veuillez saisir l'id du quiz a passer :");
		String idQuiz = MainQuiz.sc.nextLine();
		Quiz quiz = quizService.findById(idQuiz);
		Date now = new Date(System.currentTimeMillis());
		double interval = 0;
		interval = examService.durationInterval(MainQuiz.firstExam, now);

		if (student.getUserName().equals(MainQuiz.currentIdStudent)
				&& (MainQuiz.currentIdQuiz == null || !quiz.getId().equals(MainQuiz.currentIdQuiz))
				&& (interval == 0 || interval > 24)) {

			Exam exam = examService.createExam(quiz, student);

			System.out.println();
			System.out.println(" temps de realisation du test : " + exam.getDuration());

			System.out.println("resultat de votre test : " + exam.getScore() + "/" + quiz.getQuestions().size());
		} else {
			System.out.println("vous pouver refaire cet quiz seulemnt apres 24h  ");

		}

	}

	public void displayMyExams(Student student) {
		examService.dispalyStudentExams(student);

	}

}
