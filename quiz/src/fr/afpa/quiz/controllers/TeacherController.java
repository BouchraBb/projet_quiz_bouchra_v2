package fr.afpa.quiz.controllers;

import fr.afpa.quiz.MainQuiz;
import fr.afpa.quiz.models.entities.Quiz;
import fr.afpa.quiz.models.entities.Student;
import fr.afpa.quiz.services.IAuthenticationService;
import fr.afpa.quiz.services.IExamService;
import fr.afpa.quiz.services.IQuestionService;
import fr.afpa.quiz.services.IQuizService;
import fr.afpa.quiz.services.impl.AuthenticationServiceIMP;
import fr.afpa.quiz.services.impl.ExamServiceIMP;
import fr.afpa.quiz.services.impl.QuestionServiceIMP;
import fr.afpa.quiz.services.impl.QuizServiceIMP;

public class TeacherController {

	private IAuthenticationService userService = new AuthenticationServiceIMP();
	private IExamService examService = new ExamServiceIMP();
	private IQuizService quizService = new QuizServiceIMP();
	private IQuestionService questionService = new QuestionServiceIMP();

	public String principalMenu() {
		String choice;
		do {
			System.out.println("taper 1 => creer un quiz  ");
			System.out.println("taper 2 => afficher les examen d'un eleve ");
			System.out.println("taper 3 => quitter l'application ");

			choice = MainQuiz.sc.nextLine();
		} while (!("1".equals(choice)) && !("2".equals(choice)) && !("3".equals(choice)));
		return choice;
	}

	public void createQuiz() {
	
		System.out.println("veuillez saisir le nombre de questions :");
		int nbrQuestions = Integer.parseInt(MainQuiz.sc.nextLine());
		String choice;
		do {
			System.out.println("taper 1 => creer un quiz contenant des questions existantes ");
			System.out.println("taper 2 => creer un quiz contenant des nouvelles questions ");

			choice = MainQuiz.sc.nextLine();
		} while (!("1".equals(choice)) && !("2".equals(choice)));

		if ("1".equals(choice)) {
			quizService.createQuiz(nbrQuestions);
			System.out.println("DEBUG: quiz creer");

		} else if ("2".equals(choice)) {
			for (int i = 0; i < nbrQuestions; i++) {
				questionService.createQuestion();
			}
			quizService.createQuiz(nbrQuestions);

		}
	}

	public void displayStudentExams() {
		System.out.println("veuillez saisir l'identifiant de l'eleve :");
		String userName = MainQuiz.sc.nextLine();
		Student student;
		try {
			student = (Student) userService.findByUserName(userName);
			examService.dispalyStudentExams(student);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
