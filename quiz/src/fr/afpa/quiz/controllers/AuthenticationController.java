package fr.afpa.quiz.controllers;

import fr.afpa.quiz.MainQuiz;
import fr.afpa.quiz.models.entities.User;
import fr.afpa.quiz.services.IAuthenticationService;
import fr.afpa.quiz.services.impl.AuthenticationServiceIMP;

public class AuthenticationController {

	private IAuthenticationService authenticationService = new AuthenticationServiceIMP();

	public User authenticate() throws Exception {
		System.out.println("Saisissez votre login :");
		String login = MainQuiz.sc.nextLine();

		System.out.println("Saisissez votre mot de passe :");
		String password = MainQuiz.sc.nextLine();

		return authenticationService.authenticate(login, password);

	}
}
