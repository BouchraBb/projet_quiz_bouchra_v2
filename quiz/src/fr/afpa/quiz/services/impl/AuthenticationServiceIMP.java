package fr.afpa.quiz.services.impl;

import fr.afpa.quiz.dao.IAuthenticationDao;
import fr.afpa.quiz.dao.Impl.AuthenticationDaoIMP;
import fr.afpa.quiz.models.entities.User;
import fr.afpa.quiz.services.IAuthenticationService;

public class AuthenticationServiceIMP implements IAuthenticationService {


    private IAuthenticationDao authenticationDao = new AuthenticationDaoIMP();

    @Override
    public User authenticate(String username, String password) throws Exception {
        return authenticationDao.authenticate(username, password);
    }

	@Override
	public User findByUserName(String username) throws Exception {
		 return authenticationDao.findByUserName(username);
	}
}
