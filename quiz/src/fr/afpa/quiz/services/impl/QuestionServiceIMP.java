package fr.afpa.quiz.services.impl;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.quiz.MainQuiz;
import fr.afpa.quiz.dao.IQuestionDao;
import fr.afpa.quiz.dao.Impl.QuestionDaoIMP;
import fr.afpa.quiz.models.entities.Choice;
import fr.afpa.quiz.models.entities.Question;
import fr.afpa.quiz.services.IQuestionService;
import fr.afpa.quiz.utils.Constantes;
import fr.afpa.quiz.utils.FileUtils;

public class QuestionServiceIMP implements IQuestionService {
	private IQuestionDao questionDao = new QuestionDaoIMP();

	public List<Question> questionsList = new ArrayList<Question>();

	@Override
	public void createQuestion() {

		System.out.println("Saisissez l'ennoce de la question :");
		String announce = MainQuiz.sc.nextLine();

		ArrayList<Choice> choices = new ArrayList<Choice>();
		System.out.println("Reponses :");
		System.out.println("Saisissez le 1er choix :");
		Choice choice1 = new Choice(MainQuiz.sc.nextLine());
		choices.add(choice1);

		System.out.println("Saisissez le 2em choix :");
		Choice choice2 = new Choice(MainQuiz.sc.nextLine());
		choices.add(choice2);

		System.out.println("Saisissez le 3em choix :");
		Choice choice3 = new Choice(MainQuiz.sc.nextLine());
		choices.add(choice3);

		System.out.println("Saisissez le 4em choix :");
		Choice choice4 = new Choice(MainQuiz.sc.nextLine());
		choices.add(choice4);

		Choice correctChoice = null;
		String choixSeq;
		do {
			System.out.println("Saisissez le numero du choix correct parmis les choix saisies  :");
			System.out.println("taper 1 => 1er choix ");
			System.out.println("taper 2 => 2em choix ");
			System.out.println("taper 3 => 3em choix ");
			System.out.println("taper 4 => 4em choix ");

			choixSeq = MainQuiz.sc.nextLine();
		} while (!("1".equals(choixSeq)) && !("2".equals(choixSeq)) && !("3".equals(choixSeq))
				&& !("4".equals(choixSeq)));

		if ("1".equals(choixSeq)) {
			correctChoice = choice1;

		} else if ("2".equals(choixSeq)) {
			correctChoice = choice2;

		} else if ("3".equals(choixSeq)) {
			correctChoice = choice3;

		} else if ("4".equals(choixSeq)) {
			correctChoice = choice4;

		}
		FileUtils.writeToFile(Constantes.CHOICE_FILE, choices);

		Question question = new Question(announce, choices, correctChoice);
		questionsList.add(question);
		FileUtils.writeToFile(Constantes.QUESTION_FILE, questionsList);
	}

	@Override
	public void dispalyAllQuestions() {
		List<Question> questions = questionDao.findAllQuestions();
		for (Question question : questions) {
			System.out.println( question.toString1());
		}
	}

	@Override
	public Question findById(String id) {
		try {
			return questionDao.findById(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void initDataQuestion() {

		{
			String announce = "iOS est un syst�me d'exploitation propos� par�";

			ArrayList<Choice> choices = new ArrayList<Choice>();
			Choice choice1 = new Choice("Google");
			choices.add(choice1);
			Choice choice2 = new Choice("Apple");
			choices.add(choice2);
			Choice choice3 = new Choice("Microsoft");
			choices.add(choice3);
			Choice choice4 = new Choice("intel");
			choices.add(choice4);
			Choice correctChoice = choice2;

			Question question = new Question(announce, choices, correctChoice);
			questionsList.add(question);
		}

		{
			String announce = "Quel est le poids moyen de iOS ?";

			ArrayList<Choice> choices = new ArrayList<Choice>();
			Choice choice1 = new Choice("5G");
			choices.add(choice1);
			Choice choice2 = new Choice("1G");
			choices.add(choice2);
			Choice choice3 = new Choice("500 Mo");
			choices.add(choice3);
			Choice choice4 = new Choice("5 Mo");
			choices.add(choice4);
			Choice correctChoice = choice3;

			Question question = new Question(announce, choices, correctChoice);
			questionsList.add(question);
		}

		{
			String announce = "En quelle ann�e Apple a �voqu� pour la premi�re fois le terme � iOS � ?";

			ArrayList<Choice> choices = new ArrayList<Choice>();
			Choice choice1 = new Choice("en 2007");
			choices.add(choice1);
			Choice choice2 = new Choice("en 2008");
			choices.add(choice2);
			Choice choice3 = new Choice("en 2010 ");
			choices.add(choice3);
			Choice choice4 = new Choice("en 2014");
			choices.add(choice4);
			Choice correctChoice = choice1;

			Question question = new Question(announce, choices, correctChoice);
			questionsList.add(question);
		}

		{
			String announce = "Un d�veloppeur doit-il payer pour voir son application propos�e sur l'AppStore ?";

			ArrayList<Choice> choices = new ArrayList<Choice>();
			Choice choice1 = new Choice("oui");
			choices.add(choice1);
			Choice choice2 = new Choice("Cela depend de sa renommee");
			choices.add(choice2);
			Choice choice3 = new Choice("non ");
			choices.add(choice3);
			Choice choice4 = new Choice("25 dollars par compte et a vie");
			choices.add(choice4);
			Choice correctChoice = choice1;

			Question question = new Question(announce, choices, correctChoice);
			questionsList.add(question);
		}

		FileUtils.writeToFile(Constantes.QUESTION_FILE, questionsList);

	}

}
