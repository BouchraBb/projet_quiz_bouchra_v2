package fr.afpa.quiz.services.impl;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.quiz.MainQuiz;
import fr.afpa.quiz.dao.IQuizDao;
import fr.afpa.quiz.dao.Impl.QuizDaoIMP;
import fr.afpa.quiz.models.entities.Question;
import fr.afpa.quiz.models.entities.Quiz;
import fr.afpa.quiz.services.IQuestionService;
import fr.afpa.quiz.services.IQuizService;
import fr.afpa.quiz.utils.Constantes;
import fr.afpa.quiz.utils.FileUtils;

public class QuizServiceIMP implements IQuizService {
	private IQuizDao quizDao = new QuizDaoIMP();
	IQuestionService questionService = new QuestionServiceIMP();

	public List<Quiz> quizesList = new ArrayList<Quiz>();

	@Override
	public void createQuiz(int nbrQuestions) {
		Quiz quiz = new Quiz();
		Question question;
		questionService.dispalyAllQuestions();
		
		for (int i = 0; i < nbrQuestions; i++) {
			System.out.println("Veuillez saisir l'id de la question " + (i+1));
			question = questionService.findById(MainQuiz.sc.nextLine().trim());
			quiz.getQuestions().add(question);
		}
		quizesList.add(quiz);
		FileUtils.writeToFile(Constantes.QUIZ_FILE, quizesList);
	}

	@Override
	public void dispalyAllQuiz() {
		List<Quiz> quizes = quizDao.findyAllQuiz();
		for (Quiz quiz : quizes) {
			System.out.println(quiz.getId()+" , contient : "+ quiz.getQuestions().size());
		}
	}

	@Override
	public Quiz findById(String id) {
		try {
			return quizDao.findById(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void correctionQuiz(Quiz quiz) {
		for (Question question : quiz.getQuestions()) {
			System.out.println(question.correction());
		}

	}

}
