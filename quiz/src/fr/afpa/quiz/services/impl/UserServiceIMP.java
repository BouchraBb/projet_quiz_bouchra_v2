package fr.afpa.quiz.services.impl;

import java.util.ArrayList;

import fr.afpa.quiz.MainQuiz;
import fr.afpa.quiz.models.entities.Administrator;
import fr.afpa.quiz.models.entities.Student;
import fr.afpa.quiz.models.entities.Teacher;
import fr.afpa.quiz.models.entities.User;
import fr.afpa.quiz.utils.Constantes;
import fr.afpa.quiz.utils.FileUtils;

public class UserServiceIMP implements fr.afpa.quiz.services.IUserService {

	private ArrayList<User> usersList = new ArrayList<User>();

	@Override
	public void createUser() {
		String choixSeq;
		User user = null;
		do {
			System.out.println("taper 1 => creer un compte etudiant ");
			System.out.println("taper 2 => creer un compte professeur");
			System.out.println("taper 3 => creer un compte admin ");

			choixSeq = MainQuiz.sc.nextLine();
		} while (!("1".equals(choixSeq)) && !("2".equals(choixSeq)) && !("3".equals(choixSeq)));

		System.out.println("Saisissez votre login :");
		String login = MainQuiz.sc.nextLine();

		System.out.println("Saisissez votre mot de passe :");
		String password = MainQuiz.sc.nextLine();

		if ("1".equals(choixSeq)) {
			user = new Student(login, password);

		} else if ("2".equals(choixSeq)) {
			user = new Teacher(login, password);

		} else if ("3".equals(choixSeq)) {
			user = new Administrator(login, password);
		}

		usersList.add(user);
		FileUtils.writeToFile(Constantes.USER_FILE, usersList);

	}

	@Override
	public void deleteUser() {
	}

	@Override
	public void initDataUser() {
		User user = null;

		for (int i = 0; i < 16; i++) {
			user = new Student();
			usersList.add(user);
		}

		for (int i = 0; i < 5; i++) {
			user = new Teacher();
			usersList.add(user);
		}
		for (int i = 0; i < 2; i++) {
			user = new Administrator();
			usersList.add(user);
		}

		FileUtils.writeToFile(Constantes.USER_FILE, usersList);

	}



}
