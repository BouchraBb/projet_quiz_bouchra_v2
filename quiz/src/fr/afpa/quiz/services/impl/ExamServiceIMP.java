package fr.afpa.quiz.services.impl;

import java.util.Date;
import java.util.List;

import fr.afpa.quiz.MainQuiz;

import fr.afpa.quiz.dao.IExamDao;
import fr.afpa.quiz.dao.Impl.ExamDaoIMP;
import fr.afpa.quiz.models.entities.Exam;
import fr.afpa.quiz.models.entities.Question;
import fr.afpa.quiz.models.entities.Quiz;
import fr.afpa.quiz.models.entities.Student;
import fr.afpa.quiz.services.IExamService;
import fr.afpa.quiz.utils.Constantes;
import fr.afpa.quiz.utils.FileUtils;

public class ExamServiceIMP implements IExamService {
	IExamDao examDao = new ExamDaoIMP();
	public List<Exam> examsList = examDao.findAllExam();

	@Override
	public Exam createExam(Quiz quiz, Student student) {
		
		Exam exam = new Exam(quiz, student);
		MainQuiz.currentIdQuiz= quiz.getId();
		int score = 0;
		Date beginning = new Date(System.currentTimeMillis());
		//exam.set
		MainQuiz.firstExam= beginning;
		
		String studentChoice = null;
		
		for (Question question : exam.getQuiz().getQuestions()) {

			System.out.println(question.toString());
			do {
				studentChoice = MainQuiz.sc.nextLine();

				if (!("1".equals(studentChoice)) && !("2".equals(studentChoice)) && !("3".equals(studentChoice))
						&& !("4".equals(studentChoice))) {
					System.out.println(" votre choix n'est pas correct ! Resseez");
				}

			} while (!("1".equals(studentChoice)) && !("2".equals(studentChoice)) && !("3".equals(studentChoice))
					&& !("4".equals(studentChoice)));

			if ("1".equals(studentChoice)) {
				question.setAnswer(question.getChoices().get(0));

			} else if ("2".equals(studentChoice)) {
				question.setAnswer(question.getChoices().get(1));

			} else if ("3".equals(studentChoice)) {
				question.setAnswer(question.getChoices().get(2));

			} else if ("4".equals(studentChoice)) {
				question.setAnswer(question.getChoices().get(3));
			}
			// corriger et verivier reponse
			System.out.println(" La bonne reponse est : ");
			System.out.println(question.getCorrectChoice().getChoieString());
			if (question.getAnswer().equals(question.getCorrectChoice())) {
				exam.setScore(++score);
			}
		}
		// calculer la duree
		Date end = new Date(System.currentTimeMillis());
		exam.setDuration(duration(beginning, end));

		examsList.add(exam);
		FileUtils.writeToFile(Constantes.EXAM_FILE, examsList);

		return exam;
	}

	@Override
	public void dispalyStudentExams(Student student) {
		try {
			List<Exam> studentExams = examDao.findByStudent(student.getUserName());
			for (Exam exam : studentExams) {
				System.out.println(exam.toString());
			}
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public String duration(Date dateDebut, Date dateFin) {
		Date duree = new Date();

		duree.setTime(dateFin.getTime() - dateDebut.getTime());

		double secondes = duree.getTime() / 1000;
		double minutes = secondes / 60;
		secondes -= minutes * 60;

		return "" + minutes + " m: " + secondes + "s";

	}

	public double durationInterval(Date dateDebut, Date dateFin) {
		Date duree = new Date();
		if(dateDebut==null) {
			return 0;
		}
		duree.setTime(dateFin.getTime() - dateDebut.getTime());
		double houres = ((duree.getTime()/ 1000)/60)/60;

		return houres;

	}

}
