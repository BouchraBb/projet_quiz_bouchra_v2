package fr.afpa.quiz.services;

import fr.afpa.quiz.models.entities.Choice;

public interface IChoiceService {
	
	public void createChoice();
	public void dispalyAllChoice();
	public Choice findById(String id);

}
