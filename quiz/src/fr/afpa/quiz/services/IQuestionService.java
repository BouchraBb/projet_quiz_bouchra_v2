package fr.afpa.quiz.services;

import fr.afpa.quiz.models.entities.Question;

public interface IQuestionService {

	public void createQuestion();
	public void dispalyAllQuestions();
	public Question findById(String id);
}
