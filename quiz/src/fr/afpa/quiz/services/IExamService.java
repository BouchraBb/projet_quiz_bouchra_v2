package fr.afpa.quiz.services;

import fr.afpa.quiz.models.entities.Exam;
import fr.afpa.quiz.models.entities.Quiz;
import fr.afpa.quiz.models.entities.Student;

public interface IExamService {

	public Exam createExam(Quiz quiz, Student student);

	public void dispalyStudentExams(Student student);
}
