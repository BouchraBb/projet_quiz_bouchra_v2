package fr.afpa.quiz.services;

import fr.afpa.quiz.models.entities.User;

public interface IAuthenticationService {
    public User authenticate(String username, String password) throws Exception;
    public User findByUserName(String username) throws Exception;
  
}
