package fr.afpa.quiz.services;

import fr.afpa.quiz.models.entities.Quiz;

public interface IQuizService {
	
	public void createQuiz(int questions);
	public void dispalyAllQuiz();
	public Quiz findById(String id);
	public void correctionQuiz(Quiz quiz);

}
