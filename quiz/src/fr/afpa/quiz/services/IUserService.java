package fr.afpa.quiz.services;

public interface IUserService {

	public void createUser();

	public void deleteUser();

	public void initDataUser();

}
