package fr.afpa.quiz.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {

	public static <T> void writeToFile(String fileName, List<T> content) {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName));
			oos.writeObject(content);
			oos.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static <T> List<T> readFromFile(String fileName) {
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));
			List<T> data = (ArrayList<T>) ois.readObject();
			ois.close();
			return data;
		} catch (IOException | ClassNotFoundException e) {
			return new ArrayList<>();
		}
	}

}
