package fr.afpa.quiz.utils;

public class Constantes {

   public static final String  USER_FILE = "users.txt";
   public static final String  CHOICE_FILE = "choices.txt";
   public static final String  QUESTION_FILE = "questions.txt";
   public static final String  QUIZ_FILE = "quizes.txt";
   public static final String  EXAM_FILE = "exams.txt";
  
}
