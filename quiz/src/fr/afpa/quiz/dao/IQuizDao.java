package fr.afpa.quiz.dao;

import java.util.List;

import fr.afpa.quiz.models.entities.Quiz;

public interface IQuizDao {

	public List<Quiz> findyAllQuiz();

	public Quiz findById(String id) throws Exception;

}
