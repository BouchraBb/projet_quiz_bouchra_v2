package fr.afpa.quiz.dao;

import fr.afpa.quiz.models.entities.User;

public interface IAuthenticationDao{
    public User authenticate(String userName, String password) throws Exception;
    public User findByUserName(String userName) throws Exception;
}
