package fr.afpa.quiz.dao.Impl;

import java.util.List;
import java.util.Objects;

import fr.afpa.quiz.dao.IQuizDao;
import fr.afpa.quiz.models.entities.Quiz;
import fr.afpa.quiz.utils.Constantes;
import fr.afpa.quiz.utils.FileUtils;

public class QuizDaoIMP implements IQuizDao {
	List<Quiz> quizesList = (List<Quiz>) FileUtils.<Quiz>readFromFile(Constantes.QUIZ_FILE);

	@Override
	public List<Quiz> findyAllQuiz() {
		return quizesList;
	}

	@Override
	public Quiz findById(String id) throws Exception {
		
		Quiz quiz = quizesList.stream().filter(questionElement -> questionElement.getId().equals(id)).findFirst()
				.orElse(null);

		if (Objects.isNull(quiz)) {
			throw new Exception("Identifiants incorrect");
		}

		return quiz;
	}

}
