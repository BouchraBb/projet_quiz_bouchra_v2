package fr.afpa.quiz.dao.Impl;

import java.util.List;
import java.util.Objects;

import fr.afpa.quiz.dao.IQuestionDao;
import fr.afpa.quiz.models.entities.Question;
import fr.afpa.quiz.utils.Constantes;
import fr.afpa.quiz.utils.FileUtils;

public class QuestionDaoIMP implements IQuestionDao {
	
	List<Question> questionsList = (List<Question>) FileUtils.<Question>readFromFile(Constantes.QUESTION_FILE);

	@Override
	public List<Question> findAllQuestions() {
		return questionsList;
	}

	@Override
	public Question findById(String id) throws Exception {

		Question question = questionsList.stream()
				.filter(questionElement -> questionElement.getId().equals(id)).findFirst().orElse(null);

		if (Objects.isNull(question)) {
			throw new Exception("Identifiants incorrect");
		}

		return question;

	}

}
