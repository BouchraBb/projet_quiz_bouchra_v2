package fr.afpa.quiz.dao.Impl;

import java.util.List;
import java.util.Objects;

import fr.afpa.quiz.dao.IAuthenticationDao;
import fr.afpa.quiz.models.entities.User;
import fr.afpa.quiz.utils.Constantes;
import fr.afpa.quiz.utils.FileUtils;

public class AuthenticationDaoIMP implements IAuthenticationDao {


    @Override
    public User authenticate(String username, String password) throws Exception {

        List<User> usersList = (List<User>) FileUtils.<User>readFromFile(Constantes.USER_FILE);

        User user = usersList.stream()
        		//fonction landa
                .filter(userElement -> userElement.getUserPassword().equals(password) && userElement.getUserName().equalsIgnoreCase(username))
                .findFirst()
                .orElse(null);

        if (Objects.isNull(user)) {
            throw new Exception("Identifiants incorrect");
        }

        return user;



    }

	@Override
	public User findByUserName(String username) throws Exception {
		List<User> usersList = (List<User>) FileUtils.<User>readFromFile(Constantes.USER_FILE);

        User user = usersList.stream()
                .filter(userElement -> userElement.getUserName().equalsIgnoreCase(username))
                .findFirst()
                .orElse(null);

        if (Objects.isNull(user)) {
            throw new Exception("Identifiants incorrect");
        }

        return user;
	}
}
