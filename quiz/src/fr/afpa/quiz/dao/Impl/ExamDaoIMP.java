package fr.afpa.quiz.dao.Impl;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.quiz.dao.IExamDao;
import fr.afpa.quiz.models.entities.Exam;
import fr.afpa.quiz.utils.Constantes;
import fr.afpa.quiz.utils.FileUtils;

public class ExamDaoIMP implements IExamDao {
	List<Exam> examsList = FileUtils.<Exam>readFromFile(Constantes.EXAM_FILE);

	@Override
	public List<Exam> findAllExam() {
		return examsList;
	}

	@Override
	public List<Exam> findByStudent(String studentUserName) throws Exception {
		List<Exam> studentExams = new ArrayList<Exam>();

		for (Exam exam : examsList) {
			if (studentUserName.equals(exam.getStudent().getUserName()) ){
				studentExams.add(exam);
			}
		}
		return studentExams;
	}

}
