package fr.afpa.quiz.dao;

import fr.afpa.quiz.models.entities.Choice;

public interface IChoiceDao {
	
	public void dispalyAllChoice();
	public Choice findById(String id);

}
