package fr.afpa.quiz.dao;

import java.util.List;

import fr.afpa.quiz.models.entities.Question;

public interface IQuestionDao {

	public List<Question> findAllQuestions();
	public Question findById(String id)throws Exception;
}
