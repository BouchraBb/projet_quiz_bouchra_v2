package fr.afpa.quiz.dao;

import java.util.List;

import fr.afpa.quiz.models.entities.Exam;
import fr.afpa.quiz.models.entities.Student;

public interface IExamDao {

	public List<Exam> findAllExam();

	public List<Exam> findByStudent(String studentUserName) throws Exception;

}
