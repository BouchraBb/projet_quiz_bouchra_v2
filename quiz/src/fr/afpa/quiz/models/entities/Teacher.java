package fr.afpa.quiz.models.entities;

public class Teacher extends User {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static int intId = 0;

	public Teacher(String userName, String userPassword) {
		super();
		this.userName = userName;
		this.userPassword = userPassword;
	}
	
	public Teacher() {
		super();
		this.userName =  "teacher"+ (++intId);
		this.userPassword = "teacherPW"+ (intId);
	}


}
