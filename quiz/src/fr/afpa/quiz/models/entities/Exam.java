package fr.afpa.quiz.models.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Exam implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static int intId = 0;
	
	private String duration ;
	private Date beginning = new Date(System.currentTimeMillis());
	private String id;
	private Student student;
	private Quiz quiz;
	private Integer score;

	public Exam() {
		this.id= "review" +(++intId);
	}

	public Exam(Quiz quiz, Student student) {
		super();
		this.id =  "exam" +(++intId);
		this.quiz = quiz;
		this.student= student;
	}
	
	public String getId() {
		return id;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Quiz getQuiz() {
		return quiz;
	}

	public void setQuiz(Quiz quiz) {
		this.quiz = quiz;
	}


	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	@Override
	public String toString() {
		return "Examen :"+ "\n"
				+ " eleve =" + student.getUserName()+ "\n" 
				+ " quiz=" + quiz.toString() + "\n" 
				+ " duree=" + duration + "\n"
				+ " Resultat=" + score+"/"+ getQuiz().getQuestions().size()+ "]";
	}

	
	
	
	
	

}
