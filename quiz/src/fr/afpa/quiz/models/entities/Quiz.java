package fr.afpa.quiz.models.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Quiz implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static int intId = 0;

	private String id;
	private List<Question> questions = new ArrayList<Question>();

	public Quiz() {
		this.id = "quiz"+ (++intId);
	}

	public Quiz(List<Question> questions) {
		super();
		this.id ="quiz"+ (++intId);
		this.questions = questions;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	@Override
	public String toString() {
		return "Quiz [id=" + id + ", questions=" + questions.toString() + "]";
	}

}
