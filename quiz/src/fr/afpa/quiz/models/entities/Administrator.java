package fr.afpa.quiz.models.entities;

public class Administrator extends User {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static int intId = 0;

	public Administrator() {
		this.userName = "admin"+ (++intId);
		this.userPassword = "adminPW"+ (intId);
		
	}
	public Administrator(String userName, String userPassword) {
		super();
		this.userName = userName;
		this.userPassword = userPassword;
	}

}
