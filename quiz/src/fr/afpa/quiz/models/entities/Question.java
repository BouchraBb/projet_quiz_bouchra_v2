package fr.afpa.quiz.models.entities;

import java.io.Serializable;
import java.util.ArrayList;

public class Question implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static int intId = 0;

	private String id;
	private String announce;
	private ArrayList<Choice> choices = new ArrayList<Choice>();
	private Choice answer;
	private Choice correctChoice;

	public Question() {
		this.id = "qestion" + (++intId);
	}

	public Question(String announce, ArrayList<Choice> choices, Choice correctChoice) {
		super();
		this.id = "qestion" + (++intId);
		this.announce = announce;
		this.choices = choices;
		this.correctChoice = correctChoice;
	}

	public String getId() {
		return id;
	}

	public String getAnnounce() {
		return announce;
	}

	public void setAnnounce(String announce) {
		this.announce = announce;
	}

	public ArrayList<Choice> getChoices() {
		return choices;
	}

	public void setChoices(ArrayList<Choice> choices) {
		this.choices = choices;
	}

	public Choice getCorrectChoice() {
		return correctChoice;
	}

	public void setCorrectChoice(Choice correctChoice) {
		this.correctChoice = correctChoice;
	}

	@Override
	public String toString() {
		return "Question [" + this.announce + "\n" 
	+ "1- " + this.choices.get(0).getChoieString() + "\n" 
	+ "2- " + this.choices.get(1).getChoieString()+ "\n" 
	+ "3- " + this.choices.get(2).getChoieString() + "\n" 
	+ "4- " + this.choices.get(3).getChoieString() + "]";
	}

	public String toString1() {
		return "Question [ id: " + id + ", ennonce : " + this.announce + "]";
	}

	public String correction() {
		return "Question [ " + announce + "\n" + " reponse =" + correctChoice + "]";
	}

	public Choice getAnswer() {
		return answer;
	}

	public void setAnswer(Choice answer) {
		this.answer = answer;
	}

}
