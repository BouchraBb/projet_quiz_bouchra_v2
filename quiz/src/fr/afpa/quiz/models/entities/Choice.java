package fr.afpa.quiz.models.entities;

import java.io.Serializable;

public class Choice implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static int intId = 0;

	private String id;
	private String choieString;

	public Choice() {
		this.id = "choice" + (++intId);
	}

	public Choice(String choieString) {
		super();
		this.id = "choice" + (++intId);
		this.choieString = choieString;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getChoieString() {
		return choieString;
	}

	public void setChoieString(String choieString) {
		this.choieString = choieString;
	}

	@Override
	public String toString() {
		return "Choice [id=" + id + ", choieString=" + choieString + "]";
	}

}
