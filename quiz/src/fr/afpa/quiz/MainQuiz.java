package fr.afpa.quiz;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Scanner;

import fr.afpa.quiz.controllers.AdministratorController;
import fr.afpa.quiz.controllers.AuthenticationController;
import fr.afpa.quiz.controllers.StudentController;
import fr.afpa.quiz.controllers.TeacherController;
import fr.afpa.quiz.models.entities.Administrator;
import fr.afpa.quiz.models.entities.Student;
import fr.afpa.quiz.models.entities.Teacher;
import fr.afpa.quiz.models.entities.User;

public class MainQuiz {

	public static Scanner sc = new Scanner(System.in);

	public static String currentIdQuiz;
	public static Date firstExam;
	public static String currentIdStudent;

	

	public static void main(String[] args) {
		
		
		String jdbcURL = "jdbc:postgresql://localhost:5432/e_learning";
		String username = "postgres";
		String password = "2022SQLbouchra";

		try {
			Connection connection = DriverManager.getConnection(jdbcURL, username, password);
			System.out.println(" connected to postgresSQL serveur ");

			String sql = "select * from users";

			System.out.println("affichage de la table user ");
			System.out.println("--------------------------------------");
			Statement statement = connection.createStatement();

			ResultSet resultat = statement.executeQuery(sql);
			while (resultat.next())
				System.out.println( resultat.getString(1) + "  " + resultat.getString(2));

			connection.close();

		} catch (SQLException e) {
			System.out.println("error connection to postgresSQL serveur ");
			e.printStackTrace();
		}
	
		AuthenticationController authenticationController = new AuthenticationController();
		String choice ;
	

	// UserServiceIMP userService = new UserServiceIMP();
	//  data
	// userService.initDataUser();

	// QuestionServiceIMP questionService = new QuestionServiceIMP();
	// questionService.initDataQuestion();

	try
	{
		User user = authenticationController.authenticate();

		if (user instanceof Administrator) {
			AdministratorController adminController = new AdministratorController();
			adminController.createAcount();
		}

		else if (user instanceof Student) {
			StudentController studentController = new StudentController();
			Student student = (Student) user;
			currentIdStudent = student.getUserName();

			do {
				choice = studentController.principalMenu();
				if ("1".equals(choice)) {
					studentController.startExam(student);

				} else if ("2".equals(choice)) {
					studentController.displayMyExams(student);
				}
			} while (!"3".equals(choice));
		}

		else if (user instanceof Teacher) {
			TeacherController teacherController = new TeacherController();
			do {
				choice = teacherController.principalMenu();
				if ("1".equals(choice)) {
					teacherController.createQuiz();

				} else if ("2".equals(choice)) {
					teacherController.displayStudentExams();
				}
			} while (!"3".equals(choice));

		}

	}catch(
	Exception e)
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}

}
